package com.crisbarreiro.androidpaginationtask.helpers

import com.crisbarreiro.androidpaginationtask.data.network.model.*

private const val name = "test"

fun createResponse(startIndex: Int = 0, listSize: Int, page: Long = 1) =
    RandomUsersResponse(createUsersList(startIndex, listSize), Info(page))

fun createEmptyResponse() = RandomUsersResponse(listOf(), Info(1))

private fun createUsersList(startIndex: Int, listSize: Int = 10) =
    IntRange(startIndex, startIndex + listSize).map { createRandomUser(it) }

private fun createRandomUser(number: Int) =
    RandomUser(
        email = "${name}${number}@test.com",
        name = Name(title = "Mr", first = name, last = number.toString()),
        picture = Picture("", "", "")
    )