package com.crisbarreiro.androidpaginationtask.helpers

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.crisbarreiro.androidpaginationtask.data.local.RandomUsersDatabase
import com.crisbarreiro.androidpaginationtask.data.network.service.RandomUsersService
import com.crisbarreiro.androidpaginationtask.helpers.createResponse
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.delay

@Module
@InstallIn(ActivityRetainedComponent::class)
object FakeServiceModule {

    init {
        MockKAnnotations.init(this)

        coEvery { mockRandomUsersService.getRandomUsers(1, any()) }.returns(
            createResponse(listSize = 5, page = 1)
        )

        coEvery { mockRandomUsersService.getRandomUsers(2, any()) }.coAnswers {
            delay(2000)
            createResponse(startIndex = 15, listSize = 15, 2)
        }
    }

    @RelaxedMockK
    lateinit var mockRandomUsersService: RandomUsersService

    @Provides
    fun provideMockRandomUsersService(): RandomUsersService = mockRandomUsersService

    @Provides
    fun providesInMemorydatabase() = Room.inMemoryDatabaseBuilder(
        ApplicationProvider.getApplicationContext(),
        RandomUsersDatabase::class.java
    ).build()
}