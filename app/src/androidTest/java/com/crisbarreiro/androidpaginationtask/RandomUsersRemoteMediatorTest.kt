package com.crisbarreiro.androidpaginationtask

import androidx.paging.*
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.crisbarreiro.androidpaginationtask.data.local.RandomUsersDatabase
import com.crisbarreiro.androidpaginationtask.data.network.service.RandomUsersService
import com.crisbarreiro.androidpaginationtask.helpers.createEmptyResponse
import com.crisbarreiro.androidpaginationtask.helpers.createResponse
import com.crisbarreiro.androidpaginationtask.repository.RandomUsersRemoteMediator
import com.google.common.truth.Truth.assertThat
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@RunWith(AndroidJUnit4::class)
class RandomUsersRemoteMediatorTest {

    @MockK(relaxed = true)
    lateinit var mockService: RandomUsersService

    private val mockDb = Room.inMemoryDatabaseBuilder(
        ApplicationProvider.getApplicationContext(),
        RandomUsersDatabase::class.java
    ).build()

    private var remoteMediator: RandomUsersRemoteMediator

    private val pagingState =
        PagingState<Int, com.crisbarreiro.androidpaginationtask.data.local.model.RandomUser>(
            listOf(),
            anchorPosition = null,
            config = PagingConfig(pageSize = 10),
            leadingPlaceholderCount = 10
        )

    init {
        MockKAnnotations.init(this)
        remoteMediator = RandomUsersRemoteMediator(mockService, mockDb)
    }

    @After
    fun tearDown() {
        mockDb.clearAllTables()
    }

    @Test
    fun refreshLoadDoesNotReachEndOfPageWhenMoreDataIsPresent() = runTest {
        coEvery { mockService.getRandomUsers(any(), any()) }.returns(
            createResponse(listSize = 3)
        )

        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertThat((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached).isFalse()
    }

    @Test
    fun refreshLoadReturnsSuccessResultWhenMoreDataIsPresent() = runTest {
        coEvery { mockService.getRandomUsers(any(), any()) }.returns(
            createResponse(listSize = 3)
        )

        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertThat(result).isInstanceOf(RemoteMediator.MediatorResult.Success::class.java)
    }

    @Test
    fun refreshLoadReturnsSuccessResultWhenNoMoreDataIsPresent() = runTest {
        coEvery { mockService.getRandomUsers(any(), any()) }.returns(
            createEmptyResponse()
        )

        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertThat(result).isInstanceOf(RemoteMediator.MediatorResult.Success::class.java)
    }

    @Test
    fun refreshLoadReachesEndOfPageWhenNoMoreDataIsPresent() = runTest {
        coEvery { mockService.getRandomUsers(any(), any()) }.returns(
            createEmptyResponse()
        )

        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertThat((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached).isTrue()
    }

    @Test
    fun refreshLoadReturnsErrorResultWhenException() = runTest {
        coEvery { mockService.getRandomUsers(any(), any()) } throws IOException()

        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertThat(result).isInstanceOf(RemoteMediator.MediatorResult.Error::class.java)
    }
}