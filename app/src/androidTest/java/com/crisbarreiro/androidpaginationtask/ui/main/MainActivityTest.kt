package com.crisbarreiro.androidpaginationtask.ui.main

import androidx.paging.ExperimentalPagingApi
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.adevinta.android.barista.interaction.BaristaListInteractions
import com.adevinta.android.barista.internal.matcher.HelperMatchers.atPosition
import com.crisbarreiro.androidpaginationtask.R
import com.crisbarreiro.androidpaginationtask.data.di.DatabaseModule
import com.crisbarreiro.androidpaginationtask.data.di.ServiceModule
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@UninstallModules(ServiceModule::class, DatabaseModule::class)
@ExperimentalPagingApi
@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
@LargeTest
class MainActivityTest {

    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get: Rule(order = 1)
    val activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun testFragmentLoaded() {
        onView(withId(R.id.container)).check(matches(isDisplayed()))
        onView(withId(R.id.random_users_list)).check(matches(isDisplayed()))
    }

    @Test
    fun testFirstElementRendered() {
        Thread.sleep(50)
        onView(withId(R.id.random_users_list))
            .check(matches(atPosition(0, hasDescendant(withText("Mr test 0")))));
    }

    // FIXME: Flaky test
    @Ignore
    @Test
    fun testScrollingDownShowsLoader() {
        BaristaListInteractions.scrollListToPosition(R.id.random_users_list, 15)
        onView(withId(R.id.progress_bar)).check(matches(isDisplayed()))
    }
}