package com.crisbarreiro.androidpaginationtask.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.crisbarreiro.androidpaginationtask.data.local.RandomUsersDatabase
import com.crisbarreiro.androidpaginationtask.data.local.model.RandomUser
import com.crisbarreiro.androidpaginationtask.data.network.service.RandomUsersService

private const val RANDOM_USERS_STARTING_PAGE_INDEX = 1L

@ExperimentalPagingApi
class RandomUsersRemoteMediator(
    private val service: RandomUsersService,
    private val database: RandomUsersDatabase
) : RemoteMediator<Int, RandomUser>() {


    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, RandomUser>
    ): MediatorResult {
        val page: Long =
            state.lastItemOrNull()?.page?.let { it + 1 } ?: RANDOM_USERS_STARTING_PAGE_INDEX

        try {
            val apiResponse = service.getRandomUsers(page.toInt(), state.config.pageSize)

            val users = apiResponse.results
            val endOfPaginationReached = users.isEmpty()
            database.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    database.randomUsersDao().clearUsers()
                }
                database.randomUsersDao().insertAll(users.map {
                    RandomUser(
                        it.email,
                        it.name.title,
                        it.name.first,
                        it.name.last,
                        it.picture.thumbnail,
                        apiResponse.info.page
                    )
                })
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: Exception) {
            return MediatorResult.Error(exception)
        }

    }
}
