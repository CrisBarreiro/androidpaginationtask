package com.crisbarreiro.androidpaginationtask.repository.di

import androidx.paging.ExperimentalPagingApi
import com.crisbarreiro.androidpaginationtask.domain.boundaries.Domain
import com.crisbarreiro.androidpaginationtask.repository.RandomUsersRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@ExperimentalPagingApi
@Module
@InstallIn(ActivityRetainedComponent::class)
interface RepositoryModule {

    @Binds
    @ActivityRetainedScoped
    fun bindRandomUsersRepository(repository: RandomUsersRepository): Domain.RandomUsersRepository
}