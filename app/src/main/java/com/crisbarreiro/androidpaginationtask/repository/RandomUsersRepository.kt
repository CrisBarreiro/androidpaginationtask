package com.crisbarreiro.androidpaginationtask.repository

import androidx.paging.*
import com.crisbarreiro.androidpaginationtask.data.local.RandomUsersDatabase
import com.crisbarreiro.androidpaginationtask.data.network.service.RandomUsersService
import com.crisbarreiro.androidpaginationtask.domain.boundaries.Domain
import com.crisbarreiro.androidpaginationtask.domain.model.RandomUser
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@ExperimentalPagingApi
class RandomUsersRepository @Inject constructor(
    private val service: RandomUsersService,
    private val database: RandomUsersDatabase
) : Domain.RandomUsersRepository {

    override fun getUsers(): Flow<PagingData<RandomUser>> =
        Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            remoteMediator = RandomUsersRemoteMediator(
                service, database
            ),
            pagingSourceFactory = { database.randomUsersDao().getUsers() }
        ).flow.map {
            //TODO: Investigate if it's possible to localize names
            it.map { localUser ->
                RandomUser(
                    localUser.email,
                    localUser.title,
                    localUser.firstName,
                    localUser.lastName,
                    localUser.picture
                )
            }
        }

    companion object {
        const val NETWORK_PAGE_SIZE = 50
    }
}
