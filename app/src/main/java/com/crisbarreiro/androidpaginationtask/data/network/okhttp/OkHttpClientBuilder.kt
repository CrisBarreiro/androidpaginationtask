package com.crisbarreiro.androidpaginationtask.data.network.okhttp

import okhttp3.OkHttpClient

interface OkHttpClientBuilder {
    fun build(): OkHttpClient
}