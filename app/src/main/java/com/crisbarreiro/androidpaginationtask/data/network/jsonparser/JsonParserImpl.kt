package com.crisbarreiro.androidpaginationtask.data.network.jsonparser

import com.squareup.moshi.Moshi
import javax.inject.Inject

class JsonParserImpl @Inject constructor(): JsonParser {


    private val builder by lazy {
        Moshi.Builder()
    }

    fun build(): Moshi = builder.build()
}