package com.crisbarreiro.androidpaginationtask.data.di

import android.content.Context
import com.crisbarreiro.androidpaginationtask.BuildConfig
import com.crisbarreiro.androidpaginationtask.data.local.RandomUsersDatabase
import com.crisbarreiro.androidpaginationtask.data.network.createApi
import com.crisbarreiro.androidpaginationtask.data.network.jsonparser.JsonParserImpl
import com.crisbarreiro.androidpaginationtask.data.network.okhttp.OkHttpClientBuilder
import com.crisbarreiro.androidpaginationtask.data.network.okhttp.OkHttpClientBuilderImpl
import com.crisbarreiro.androidpaginationtask.data.network.retrofit.RetrofitClientBuilder
import com.crisbarreiro.androidpaginationtask.data.network.retrofit.RetrofitClientBuilderImpl
import com.crisbarreiro.androidpaginationtask.data.network.service.RandomUsersService
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped
import okhttp3.OkHttpClient
import retrofit2.Retrofit

@Module
@InstallIn(ActivityRetainedComponent::class)
interface DataModule {

    @Binds
    @ActivityRetainedScoped
    fun bindOkHttpClientBuilder(builder: OkHttpClientBuilderImpl): OkHttpClientBuilder

    companion object {

        @Provides
        @ActivityRetainedScoped
        fun bindRetrofitBuilder(okHttpClient: OkHttpClient, moshi: Moshi): RetrofitClientBuilder =
            RetrofitClientBuilderImpl(
                baseUrl = BuildConfig.BASE_URL,
                okHttpClient = okHttpClient,
                moshi = moshi
            )

        @Provides
        @ActivityRetainedScoped
        fun provideOkHttpClient(builder: OkHttpClientBuilder) = builder.build()

        @Provides
        @ActivityRetainedScoped
        fun provideRetrofitClient(builder: RetrofitClientBuilder) = builder.build()

        @Provides
        @ActivityRetainedScoped
        fun provideMoshi(builder: JsonParserImpl) = builder.build()
    }
}

@Module
@InstallIn(ActivityRetainedComponent::class)
object ServiceModule {

    @Provides
    @ActivityRetainedScoped
    fun provideRandomUsersService(retrofitClient: Retrofit) =
        createApi<RandomUsersService>(retrofitClient)
}

@Module
@InstallIn(ActivityRetainedComponent::class)
object DatabaseModule {
    @Provides
    @ActivityRetainedScoped
    fun provideRandomUsersDatabase(@ApplicationContext context: Context) =
        RandomUsersDatabase.getInstance(context)
}