package com.crisbarreiro.androidpaginationtask.data.network.model

data class RandomUsersResponse (
    val results: List<RandomUser>,
    val info: Info
)

data class RandomUser (
    val name: Name,
    val email: String,
    val picture: Picture,
)

data class Name (
    val title: String,
    val first: String,
    val last: String
)

data class Picture(
    val large: String,
    val medium: String,
    val thumbnail: String
)

data class Info(
    val page: Long
)