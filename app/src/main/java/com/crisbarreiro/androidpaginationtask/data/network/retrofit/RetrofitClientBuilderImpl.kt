package com.crisbarreiro.androidpaginationtask.data.network.retrofit

import com.crisbarreiro.androidpaginationtask.data.network.retrofit.RetrofitClientBuilder
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitClientBuilderImpl(
    baseUrl: String,
    okHttpClient: OkHttpClient,
    moshi: Moshi
): RetrofitClientBuilder {
    private val builder by lazy { Retrofit.Builder() }

    init {
        builder
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
    }

    override fun build() : Retrofit = builder.build()
}