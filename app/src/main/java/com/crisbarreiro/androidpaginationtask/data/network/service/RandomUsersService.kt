package com.crisbarreiro.androidpaginationtask.data.network.service

import com.crisbarreiro.androidpaginationtask.data.network.model.RandomUsersResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RandomUsersService {

    @GET("api/")
    suspend fun getRandomUsers(
        @Query("page") page: Int,
        @Query("results") loadSize: Int
    ): RandomUsersResponse
}