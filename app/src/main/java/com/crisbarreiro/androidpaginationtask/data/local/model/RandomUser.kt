package com.crisbarreiro.androidpaginationtask.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "RandomUsers")
data class RandomUser(
    @PrimaryKey val email: String,
    val title: String,
    val firstName: String,
    val lastName: String,
    val picture: String,
    val page: Long
)