package com.crisbarreiro.androidpaginationtask.data.local

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.crisbarreiro.androidpaginationtask.data.local.model.RandomUser

@Dao
interface RandomUsersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(repos: List<RandomUser>)

    @Query("SELECT * FROM RandomUsers")
    fun getUsers(): PagingSource<Int, RandomUser>

    @Query("DELETE FROM RandomUsers")
    suspend fun clearUsers()

}