package com.crisbarreiro.androidpaginationtask.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.crisbarreiro.androidpaginationtask.data.local.model.RandomUser

@Database(
    entities = [RandomUser::class],
    version = 1,
    exportSchema = false
)
abstract class RandomUsersDatabase : RoomDatabase() {

    abstract fun randomUsersDao(): RandomUsersDao

    companion object {

        @Volatile
        private var INSTANCE: RandomUsersDatabase? = null

        fun getInstance(context: Context): RandomUsersDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                RandomUsersDatabase::class.java, "RandomUsers.db"
            ).build()
    }

}