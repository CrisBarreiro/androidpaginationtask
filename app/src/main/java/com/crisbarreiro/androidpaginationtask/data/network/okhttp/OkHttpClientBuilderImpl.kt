package com.crisbarreiro.androidpaginationtask.data.network.okhttp

import com.crisbarreiro.androidpaginationtask.data.network.okhttp.OkHttpClientBuilder
import okhttp3.OkHttpClient
import javax.inject.Inject

class OkHttpClientBuilderImpl @Inject constructor(): OkHttpClientBuilder {

    private val builder by lazy {
        OkHttpClient.Builder()
    }

    override fun build(): OkHttpClient = builder.build()
}