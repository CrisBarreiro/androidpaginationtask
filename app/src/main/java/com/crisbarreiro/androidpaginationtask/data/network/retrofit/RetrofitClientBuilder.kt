package com.crisbarreiro.androidpaginationtask.data.network.retrofit

import retrofit2.Retrofit

interface RetrofitClientBuilder {
    fun build(): Retrofit
}