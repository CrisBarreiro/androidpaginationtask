package com.crisbarreiro.androidpaginationtask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AndroidPaginationTaskApplication: Application() {
}