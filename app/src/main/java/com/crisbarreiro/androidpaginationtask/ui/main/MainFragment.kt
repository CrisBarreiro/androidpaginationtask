package com.crisbarreiro.androidpaginationtask.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crisbarreiro.androidpaginationtask.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@ExperimentalPagingApi
@AndroidEntryPoint
class MainFragment : Fragment() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        val view = inflater.inflate(R.layout.main_fragment, container, false)

        val adapter = RandomUsersAdapter()

        val rv = view.findViewById<RecyclerView>(R.id.random_users_list)
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = adapter.withLoadStateFooter(
            footer = RandomUsersStateAdapter { adapter.retry() }
        )

        viewModel.users.observe(viewLifecycleOwner) {
            lifecycleScope.launch {
                adapter.submitData(it)
            }
        }

        return view
    }

    companion object {
        fun newInstance() = MainFragment()
    }

}