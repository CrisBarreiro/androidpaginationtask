package com.crisbarreiro.androidpaginationtask.ui.main

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

class RandomUsersStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<StateItemViewHolder>() {
    override fun onBindViewHolder(holder: StateItemViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): StateItemViewHolder =
        StateItemViewHolder.create(parent) { retry() }
}