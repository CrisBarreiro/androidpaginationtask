package com.crisbarreiro.androidpaginationtask.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.crisbarreiro.androidpaginationtask.R

class StateItemViewHolder(view: View, private val retryCallback: () -> Unit) :
    RecyclerView.ViewHolder(view) {

    private val progressBar = view.findViewById<ProgressBar>(R.id.progress_bar)
    private val errorMessage = view.findViewById<TextView>(R.id.error_msg)
    private val retryButton = view.findViewById<Button>(R.id.retry_button)

    init {
        retryButton.setOnClickListener { retryCallback() }
    }

    fun bind(loadState: LoadState) {
        progressBar.isVisible = loadState is LoadState.Loading
        errorMessage.isVisible = loadState is LoadState.Error
        retryButton.isVisible = loadState is LoadState.Error
    }


    companion object {
        fun create(parent: ViewGroup, retryCallback: () -> Unit): StateItemViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_loading_state, parent, false)
            return StateItemViewHolder(view, retryCallback)
        }
    }
}