/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.crisbarreiro.androidpaginationtask.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.crisbarreiro.androidpaginationtask.R
import com.crisbarreiro.androidpaginationtask.domain.model.RandomUser

/**
 * View Holder for a [RandomUser] RecyclerView list item.
 */
class RandomUsersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val name: TextView = view.findViewById(R.id.user_name)
    private val email: TextView = view.findViewById(R.id.user_email)

    private var randomUser: RandomUser? = null

    fun bind(randomUser: RandomUser) {
        this.randomUser = randomUser
        name.text = randomUser.name
        email.text = randomUser.email
    }

    companion object {
        fun create(parent: ViewGroup): RandomUsersViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.random_user_item, parent, false)
            return RandomUsersViewHolder(view)
        }
    }
}
