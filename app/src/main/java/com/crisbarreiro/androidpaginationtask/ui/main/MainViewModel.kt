package com.crisbarreiro.androidpaginationtask.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.crisbarreiro.androidpaginationtask.domain.boundaries.Domain
import com.crisbarreiro.androidpaginationtask.domain.model.RandomUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalPagingApi
@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Domain.RandomUsersRepository) :
    ViewModel() {

    private val _users = MutableLiveData<PagingData<RandomUser>>()
    val users: LiveData<PagingData<RandomUser>> get() = _users

    init {
        getUsers()
    }

    private fun getUsers() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getUsers().cachedIn(viewModelScope).collectLatest {
                _users.postValue(it)
            }
        }
    }

}