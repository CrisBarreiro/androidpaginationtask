package com.crisbarreiro.androidpaginationtask.domain.boundaries

import androidx.paging.PagingData
import com.crisbarreiro.androidpaginationtask.domain.model.RandomUser
import kotlinx.coroutines.flow.Flow

interface Domain {

    interface RandomUsersRepository {
        fun getUsers(): Flow<PagingData<RandomUser>>
    }
}