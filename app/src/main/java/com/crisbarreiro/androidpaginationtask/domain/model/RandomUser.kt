package com.crisbarreiro.androidpaginationtask.domain.model

import java.util.*

private const val pattern = "%s %s %s"

data class RandomUser(
    val email: String,
    val name: String,
    val picture: String
) {
    constructor(email: String, title: String, firstName: String, lastName: String, picture: String)
            : this(
        email,
        String.format(Locale.getDefault(), pattern, title, firstName, lastName),
        picture
    )
}