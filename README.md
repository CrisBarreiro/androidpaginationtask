# Instructions

Build an application that fetches data from this API - [https://randomuser.me](https://randomuser.me/) - and displays a list of Users. 
Start by reading the API documentation [https://randomuser.me/documentation](https://randomuser.me/documentation). Your application should 
fetch multiple pages of Users from that API and display a list of Users with first and last names on one line and the email below. Following pages should be fetched when users scroll the list.
Finally, while offline, previously loaded Users should still be accessible from the list. We recommend you implement this based on a classic database solution like Room or Realm.

Time constraints: 4h over the course of 3 days

## Code guidelines

Feel free to use any third-party libraries you'd need.
We favour quality over quantity, so here are a few things you should keep in mind:

- your project should follow a well-known design pattern (MVVM, Clean architecture, MVP, etc...)
- your code should contain some developers' good practices (SOLID, KISS, DRY, etc...)
- cover some classes with tests (no need to cover everything)
- favor technologies you master rather than new, fancy ones

## App Development

A video demoing the app can be found [here](https://photos.app.goo.gl/vovMWjekiG4SgriB6)

### Implemented features

* Fetch data from [https://randomuser.me](https://randomuser.me/)
* Implement infinite scrolling
* Display name and email for every user
* Show loading indicator in case the user reaches the end of the list and there's more content to be
  loaded
* Show error button with retry capabilities in case the user reaches the end of the list and there
  was an error loading more content
* Use a database to show previously loaded results if network results can't be loaded when opening
  the app<sup>*</sup>
* List optimizations so we can add new elements without replacing the entire list, which would be
  bad for performance.

<sup>*</sup> When opening the app, the cached version is always loaded and then we try to update the
results in the background. If this is not possible, we keep the list, but if we get new results, we
replace the contents of the list. This is done because the network service uses a random seed, so
every time we query the service, results change

### Potential improvements

* Show error indicator if the initial content loading fails
* Show a Snackbar or similar component when showing results from the database, to inform the user
  that results are not up-to-date
* Implement quick scroll
* Display user picture
* Add pull-to-refresh capabilities
* Investigate if it's possible to localize names, as some languages show first and last names in
  different order

Feature-wise, I decided to prioritize the happy path, this is, when the user actually gets data from
the network, and there's several pages to be loaded, and also the offline functionality, as it was
one of the requirements for the test.

## Known issues:

* In some cases, after the initial load, the list keeps sending request to re-load the same page
* The offline functionality has room for improvement when it comes to User Experience. If this was a
  real-life project, I would consult with my team whether we should stick to the seed of the first
  page we get or not, and, based on this, provide better feedback to the user when we're actually
  replacing data.

## Development strategy

As this was my first time implementing a list with infinite scroll, I spent some time investigating
the Android Paging library, and whether or not it was possible to use it for this task. Even if the
instructions state that I should favor technologies that I master, I decided to use the test as an
opportunity to learn a new technology and use an existing, well-known, tested, and backed by Google
technology, rather than learning how to reinvent the wheel and develop my own paging strategy.
However, this is something that slowed me down significantly, and put a lot of pressure on me, as
there was no way to solve the assignment by only using technologies I was familiar within a
reasonable and timely manner. The paging library is quite complex, and needs to be tested in a very
specific way, so I needed to learn not only how to use it, but also how to test it. All of this
stressed me quite a bit, since while the readme mentions I could take all the time I needed for 3
days to solve the challenge, the recruiter mentioned I should be able to complete it within 4 hours,
which was not realistic for me due to the need to learn a new technology. If this was a real-life
project, I would've spent more time troubleshooting the known issues, but I decided to optimize my
time and focus on other parts of the technical task. In retrospective, even if I stick to the
initial reasons why I chose to use the paging library, it would've probably been easier to develop
my own solution, especially taking into account that I would not have the time nor the resources to
properly troubleshoot issues and ask for help and support.

In order to better understand how to use the paging library, I consulted the official Android
Developers documentation
on [Paging Library](https://developer.android.com/topic/libraries/architecture/paging/v3-overview),
the [Paging Library Codelab](https://developer.android.com/codelabs/android-paging), and
the [Ray Wenderlich Paging Library tutorial](https://www.raywenderlich.com/12244218-paging-library-for-android-with-kotlin-creating-infinite-lists)
. After reading through these materials and determining the paging library was compatible with the
provided API, I investigated whether there was an integration with Room to provide offline
capabilities and, ideally, use the database as a single source of truth, so I didn't have to
integrate 2 different sources, which would made the flow more error prone. I found out the
integration was still in alpha stage, but I decided to use it anyway due to the time constraints,
and also considering that, even if it's alpha stages, it should be tested enough to work on the
default usecases.

I started by creating a very basic skeleton of the application, creating some interfaces to enforce
the dependency inversion, and also the dependency injection setup.

I chose to use [Hilt](https://dagger.dev/hilt/) for dependency injection, as it generates the graph
in compile time, which makes it safer to use and doesn't have an impact on the app performance, and
it's simpler to use than [Dagger](https://dagger.dev/dev-guide/), while providing the same
advantages, as it's built on top of it.

Initially, I decided to create the following layers:

* UI. Contains activities, fragments, views and ViewModel.
    * I decided to use MVVM for the presentation layer, as it is lifecycle-aware, which simplifies the
      process of reacting to configuration changes such as screen rotation, it's easier to test than
      other options as the ViewModel does not need a reference to the view, it's the pattern that
      Google recommends, and, more importantly for this test in particular, it fits quite well with
      the reactive nature of the infinite scroll and the Paging Library in particular.
* Repository. Facade for data retrieval. Communicates with both local and network data sources, and
  handles the paging configuration.
* Data. Contains the database and remote service implementations.

Although this architecture is inspired on Clean Architecture, it has some room for improvements (
some of which would be implemented later on the development process):

* There's no domain layer.
    * The domain model would be added later on, as at this stage I decided to prioritize having the
      list loaded for the first time.
    * There's no usecases, as I decided to prioritize other functionality and parts of the
      architecture, due to the lack of business logic on this application. However, an interesting
      improvement might be to define a base usecase that would handle the coroutine scope, switching
      back to the main thread before returning data to the UI. This way, we don't need to do this
      manually on the UI, reducing potential errors.
* The ViewModel depends directly on the Repository implementation. This would be addressed later on
  by creating a repository Interface
* Ideally, the repository should not depend directly on the Room Database, nor the Retrofit service,
  but on interfaces. Similarly, we should define a repository model. This way, the data layer would
  depend on the repository layer, implement its interfaces for the datasources, and return the
  repository model, and the repository would have no knowledge of the data layer, making it easier
  to replace these technologies.
* Use modules rather than packages for each layer, so we enforce the direction of the dependencies
* Explore the possibility to wrap the paging library components, to make the code more future-proof,
  especially taking into account the paging library integration with Room is still in alpha stage,
  which means the API is likely to change, and there has been 2 previous versions of the
  PagingLibrary with breaking changes, so there's precedent for this to happen again in the future.
  In addition to this, by wrapping the paging library and its data model, we would avoid depending
  on Android specific components on the repository layer, which is specially interesting in case
  we'd like to build a multiplatform app and have a different paging strategy for iOS.
  Alternatively, if this was a real-life scenario, I would consider manually handling the
  integration with the database so we don't depend in alpha-stage code that is likely to change.
* Have mapper functions on different files

To reduce the complexity of the implementation, I first developed an initial version that would just
fetch data from a remote, hence not providing offline capabilities, and then iterate from there.

I noticed that, when rapidly scrolling and reaching the end of the loaded results before getting the
next page, the user would not get any kind of feedback, so I decided to take advantage of the load
status reporting capabilities of the paging library, and added a load state footer that would
display a loading indicator or retry button when needed

Then I added a database, and created a `RemoteMediator` to manage both remote and local datasources.
At this point, I made some changes, to make other layers use the database model, as from then on,
this was the single source of truth. At this point, it became evident that I needed to have a common
model, so my presentation layer would not depend on models from the data layer that contained
network and database-related annotations. This would also allow me to have a data model more
friendly with the features themselves, and which is not dependent on the network and database
constraints. This also means that, in case the network model changes, I won't need to make changes
to the view layer. Similarly, as I have a different data model for the database, a change in the
data model would not automatically require a change in the database model with a subsequent need for
a migration strategy. Even if the repository depends on the actual database implementation, the
database itself is provided via dependency injection, so it can be easily mocked or faked, and the
dependency with the Android context (which is needed to create the database) is isolated to the
application module.

At this point, I decided to spend some time improving the package structure and expanding the data
models, so I could create a version of the UI that contained the required information. Up to this
point, I was just focused on getting a list to be displayed, but not that much on the UI or even the
pagination itself.

Once I made some tweaks to the UI, I focused on testing the repository layer. I decided to test this
layer in particular, as is the one in charge of controlling the behavior of the paging library
itself, send the success and error states, and determine whether there's more content to be loaded.

At this point, I decided to spend some more time refactoring the app, adding a repository interface
so the ViewModel could depend on an abstraction to enforce the Dependency Inversion principle. This
way, both the ViewModel and Repository depend on the Domain layer, but don't know each other
directly. I also did some manual testing, which led me to fix a bug that would cause the app to
crash when rotating the screen, and also optimized the Dagger setup. Initially, I had created the
modules as `SingletonComponent`, but I decided to migrate them to `ActivityRetainedComponent`. The
type of component is not relevant in this case, as the entire application only contains one
Activity, so in reality the application scope (`SingletonComponent`) and the ViewModel
scope (`ActivityRetainedComponent`) would behave the same and have the same, but this is a risk in
case new activities are added, and we forget to change the component type, because in that case the
activity dependencies would be alive until the application dies, even if the activity is not alive
anymore, so I decided to change it.

Once I was done with this improvements, I decided to start writing some integration tests. However,
while I was doing some debugging and profiling of the app to understand why my tests were failing, I
realized the application was not working as intended, as in some cases only the first page was being
loaded, even if I didn't notice during manual testing. Once I got this error fixed, I continued
working on my integration testing strategy. However, I wasn't fully able to fix these issues, as
some pages are requested more than once, but as stated earlier, I wasn't able to find a proper fix
and decided to move forward with testing. Of course, on a real-time project I would spend time
properly debugging, asking my colleagues for help, and browsing online resources.

For integration testing, I decided to mock both the service and the database, so external conditions
such as network connectivity or previous database status wouldn't change the test results. Mocking
other layers closer to the view might have been more correct, but I decided to do this for the
following reasons:

* The mediator was already tested and I had a good level of confidence on it working correctly
* Mocking these components was the easiest way to not depend on external factors while keeping the
  paging library capabilities

In order to mock these components, I needed to disable the Hilt module that contained them, and
create a new module capable of providing them. To simplify the setup, I moved each of these
components to a new Dagger module, so I could focus on just mocking these components, and not the
rest of the components on the `DataModule`. To mock these components, I decided to
use [MockK](https://mockk.io/), which is the recommended library for Kotlin, and an in-memory
database. My initial goal was to test whether `MainFragment` was loaded, whether the first component
of the list was displayed correctly, and and whether the loading indicator was displayed after
scrolling to the bottom of the list, but unfortunately, the latter turned out to be a flaky test.

For testing, I tried to follow
the [ObjectMother pattern](https://martinfowler.com/bliki/ObjectMother.html), so I could easily
create accessory testing data without having to repeat a lot of code, and focusing only on the
parameters relevant to the test. This also makes it easier to maintain the tests, as in case there's
a change on the data model, we might be able to address it just on `RandomUserTestHelper`, rather
than on each individual test.

## Commit strategy

As a general rule, the commit strategy follows the development strategy. For minor issues, I tried
to amend or fixup previous commits, but in some cases I needed to address issues as separate commits
to be able to focus on the development and testing rather than solving conflicts. However, I tried
to make sure the app was able to build an run before committing.

## Key learnings

Looking back, I realize I've learnt a few things, and there's some others that I now acknowledge I
should've done differently:

* Test the functionality in depth earlier on. While I first focused on having a very basic working
  version on the list, I didn't realize that, in some cases, only the first page was being loaded.
  Once I got this basic version to work, I should've spent some time not only manually testing the
  app, but also running the network inspector to make sure the network call stack was correct.
* Write tests earlier. I was probably too focused on having a fully working version before starting
  to write tests, but I should've probably test the repository layer once I was pretty confident I
  was done with it, and not after tweaking the UI.
* Using a new technology for a coding task on a hiring process is hard. Even if a well-known and
  well-tested solution should be the obvious choice on a production environment, it might not be
  helpful at all on an already stressful and time-constrained environment such as a technical task.